package database.Query;

import java.sql.*;

public class DeleteUserQuery implements Query{
	private PreparedStatement stmt = null;
	
	public DeleteUserQuery(Connection conn, String userEmail) {
		try {
			stmt = conn.prepareStatement("DELETE FROM users WHERE user_name = ?");
			stmt.setString(1, userEmail);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void execute() {
		try {
			stmt.execute();
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void close() {
		try {
			stmt.close();
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}

}
