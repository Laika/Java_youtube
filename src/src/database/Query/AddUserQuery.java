package database.Query;

import java.sql.*;

public class AddUserQuery implements Query {
	private PreparedStatement stmt = null;
	
	public AddUserQuery(Connection conn, String userEmail, String userPassword, String userSalt, String clientHash, String clientSalt) {
		try {
			stmt = conn.prepareStatement("INSERT INTO users (user_name, pw_hash, pw_client_hash, pw_salt, pw_client_salt) VALUES (?, ?, ?, ?, ?)");
			stmt.setString(1, userEmail);
			stmt.setString(2, userPassword);
			stmt.setString(3, clientHash);
			stmt.setString(4, userSalt);
			stmt.setString(5, clientSalt);
			
			System.out.println(userEmail + " " + userPassword + " " + clientHash + " " + userSalt + " " + clientSalt);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void execute() {
		try {
			stmt.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void close () {
		try {
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
