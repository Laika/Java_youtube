package database.Query;

import java.sql.*;

public class VideoExistsQuery implements Query {
	private PreparedStatement stmt = null;
	private ResultSet  rs = null;
	
	public VideoExistsQuery(Connection conn, String vid) {
		try {
			stmt = conn.prepareStatement("SELECT exists (SELECT 1 FROM videos WHERE video_id = ? LIMIT 1)");
			stmt.setString(1, vid);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void execute() {
		try {
			rs = stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ResultSet getResults() {
		return rs;
	}
	
	@Override
	public void close() {
		try {
			stmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
