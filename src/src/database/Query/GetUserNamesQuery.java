package database.Query;

import java.sql.*;

public class GetUserNamesQuery implements Query {
	private PreparedStatement stmt = null;
	private ResultSet rs = null;
	
	public GetUserNamesQuery(Connection conn) {
		try {
			stmt = conn.prepareStatement("SELECT user_name FROM users");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void execute() {
		try {
			rs = stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ResultSet getResults() {
		return rs;
	}
	
	public void close () {
		try {
			stmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
