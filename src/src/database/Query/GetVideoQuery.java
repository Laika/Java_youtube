package database.Query;

import java.sql.*;

public class GetVideoQuery implements Query {
	
	private PreparedStatement stmt = null;
	private ResultSet rs = null;
	
	public GetVideoQuery(Connection conn, int videoId) {
		try {
			stmt = conn.prepareStatement("SELECT video_url, thumbnail_url, description, title, duration FROM videos WHERE id = ? LIMIT 1");
			stmt.setInt(1, videoId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void execute() {
		try {
			rs = stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ResultSet getResults() {
		return rs;
	}
	
	public void close() {
		try {
			stmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
