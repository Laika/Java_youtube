package database.Query;

import java.sql.*;

public class GetAllVideosQuery implements Query {
	
	private PreparedStatement stmt = null;
	private ResultSet rs = null;
	
	public GetAllVideosQuery(Connection conn) {
		try {
			stmt = conn.prepareStatement("SELECT * FROM videos");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ResultSet getResults() {
		return rs;
	}
	
	public void execute() {
		try {
			rs = stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void close() {
		try {
			stmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
