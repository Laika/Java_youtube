package database.Query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GetFrontPageVideosQuery implements Query {


	private PreparedStatement stmt = null;
	private ResultSet rs = null;
	
	public GetFrontPageVideosQuery(Connection conn) {
		try {
			stmt = conn.prepareStatement("SELECT * FROM videos ORDER BY RANDOM() LIMIT 6");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void execute() {
		try {
			rs = stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void close() {
		try {
			stmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ResultSet getResults() {
		return rs;
	}

}
