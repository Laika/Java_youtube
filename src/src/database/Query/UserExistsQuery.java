package database.Query;

import java.sql.*;

public class UserExistsQuery implements Query {
	private PreparedStatement stmt = null;
	private ResultSet rs = null;
	
	public UserExistsQuery(Connection conn, String userEmail) {
		try {
			stmt = conn.prepareStatement("SELECT exists (SELECT 1 FROM users WHERE user_name = ? LIMIT 1)");
            stmt.setString(1, userEmail);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void execute() {
		// TODO Auto-generated method stub
		try {
			rs = stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public ResultSet getResults() {
		return rs;
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		try {
			stmt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
