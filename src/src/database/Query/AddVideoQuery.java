package database.Query;

import java.sql.*;

public class AddVideoQuery implements Query {
	private PreparedStatement stmt = null;
	
	public AddVideoQuery(Connection conn, String thumb_url, String video_url, String description, String title, String duration, String st, String videoid) {
		try {
			stmt = conn.prepareStatement("INSERT INTO videos (thumbnail_url, video_url, description, title, duration, searchterm, video_id) VALUES (?, ?, ?, ?, ?, ?, ?)");
			stmt.setString(1, thumb_url);
			stmt.setString(2, video_url);
			stmt.setString(3, description);
			stmt.setString(4, title);
			stmt.setString(5, duration);
			stmt.setString(6, st);
			stmt.setString(7, videoid);
			
			System.out.println(videoid + " " + thumb_url + " " + video_url + " " + description + " " + title + " " + duration + " " + st);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void execute() {
		try {
			stmt.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void close () {
		try {
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}