package database;

import java.sql.*;

public class DBConnection {
	private final String dbName = "aggregator";
	private final String dbUserName = "aggregator_role";
	private final String dbUserPassword = "aggregator_role1";
	
	private Connection dbConnection;
	
	public DBConnection() {
		dbConnection = null;
		
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void connect() {
		try {
			dbConnection = DriverManager.getConnection("jdbc:postgresql:" + dbName, dbUserName, dbUserPassword);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void disconnect() {
		try {
			dbConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Connection getDbConnection() {
		return dbConnection;
	}
}
