package logic;

public class VideoData {	
	public String thumbnailUrl;
	public String videoUrl;
	public String description;
	public String title;
	public String duration;
	public String searchTerm;
	public String videoId;
	
	public VideoData(String thumbnailUrl, String videoUrl) {
		this.thumbnailUrl = thumbnailUrl;
		this.videoUrl = videoUrl;
	}
	
	public VideoData(String thumbnailUrl, String videoUrl, String description, String title, String duration) {
		this.thumbnailUrl = thumbnailUrl;
		this.videoUrl = videoUrl;
		this.description = description;
		this.title = title;
		this.duration = duration;
	}
	
	public VideoData(String thumbnailUrl, String videoUrl, String description, String title, String duration, String searchTerm, String video_id)
	{
		this.thumbnailUrl = thumbnailUrl;
		this.videoUrl     = videoUrl;
		this.description  = description;
		this.title        = title;
		this.duration     = duration;
		this.searchTerm   = searchTerm;
		this.videoId     = video_id;
	}
}