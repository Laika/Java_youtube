package logic;

import database.*;

public class Logic {
	private UserManager userManager;
	private DBConnection db;
	
	public Logic(DBConnection db) {
		this.db = db;
		this.userManager = new UserManager(db);
	}
	
	public UserManager getUserManager() {
		return userManager;
	}
	
	public DBConnection getDb() {
		return db;
	}
	
	public boolean verifyPassword() {
		return true;
	}

}
