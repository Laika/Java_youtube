package logic;

public class DisplayVideos {
	
	private int id;
	private String thumbnailUrl;
	private String videoUrl;
	private String description;
	private String title;
	private String duration;
	private String searchTerm;
	private String videoId;
	
	public DisplayVideos( int id, String t, String v, String d, String ttl, String dur, String st, String vid) {
		this.id = id;
		this.thumbnailUrl = t;
		this.videoUrl = v;
		this.description = d;
		this.title = ttl;
		this.duration = dur;
		this.searchTerm = st;
		this.videoId = vid;
	}
	
	@Override
	public String toString() {
		return "Title: " + title + "\nDuration: " + duration + "\nDescription: " + description + "\nVideo url: " + videoUrl + "\nThumbnail url: " + thumbnailUrl + "\nId: " + id + "\nVideo id: " + videoId + "\nSearch term: " + searchTerm + "\n--------------------------------\n\n";
	}

}
