package logic.ResponseData;


public class RegisterUserResponseData implements ResponseData {
	
	private boolean ok;
	private boolean userNameTaken;

	public RegisterUserResponseData( boolean ok, boolean userNameTaken ) {
		this.ok = ok;
		this.userNameTaken = userNameTaken;
	}

	@Override
	public String getResponseData() {
		return new String("ok: " + ok + "; userNameTaken: " + userNameTaken + ";");
	}

}
