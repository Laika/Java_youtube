package logic.ResponseData;


public class NoSuchUserResponseData implements ResponseData {

	private String user;
	
	public NoSuchUserResponseData(String username) {
		user = username;
	}

	@Override
	public String getResponseData() {
		// TODO Auto-generated method stub
		return user;
	}

}
