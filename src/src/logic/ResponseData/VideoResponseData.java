package logic.ResponseData;

import java.util.List;

import logic.VideoData;


import client.YoutubeVideo;

public class VideoResponseData implements ResponseData {
	
	private VideoData[] data;
	
	public VideoResponseData(List<YoutubeVideo> videos) {

		this.data = new VideoData[videos.size()];
		
		for (int i = 0; i < videos.size(); i++) {
			YoutubeVideo v = videos.get(i);
			data[i] = new VideoData(v.getThumbnails().get(1), v.getEmbeddedWebPlayerUrl(), v.getDescription(), v.getTitle(), v.getDuration());
		}
	}
	
	public VideoResponseData(String t, String v) {
		this.data = new VideoData[1];
		data[0] = new VideoData(t, v);
	}
	
	@Override
	public String getResponseData() {
		return null;
	}
	
}
