package logic;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.*;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class PasswordHash {
	/*
	 * 
	 * Derived from:
	 * https://www.owasp.org/index.php/Hashing_Java
	 * 
	 */
	private static final int ITERATIONS = 1000;	
	private SecureRandom random;

	private String salt;
	private String digest;
	
	public PasswordHash() {
		try {
			random = SecureRandom.getInstance("SHA1PRNG");
		} catch (NoSuchAlgorithmException e) {
			/*
			 * all breaks down if we can't hash things reliably
			 */
			e.printStackTrace();
		}
	}
	
	public void Hash(String password) {
		byte[] salt = new byte[8];	
		random.nextBytes(salt);
		
		byte[] digest = getHash(ITERATIONS, password, salt);
		
		this.digest = byteToBase64(digest);
		this.salt   = byteToBase64(salt);
	}
	
	public void Hash(String password, byte[] salt) {
		byte[] digest = getHash(ITERATIONS, password, salt);
		
		this.digest = byteToBase64(digest);
		this.salt   = byteToBase64(salt);
	}
	
	public byte[] getHash(int iterations, String password,  byte[] salt) {
		MessageDigest digest = null;
		
		try {
			digest = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		digest.reset();
		digest.update(salt);
		
		byte[] input = null;
		
		try {
			input = digest.digest(password.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		for (int i = 0; i < iterations; i++) {
			digest.reset();
			input = digest.digest(input);
		}
		return input;
	}
	
	public static byte[] base64ToByte(String data) throws IOException {
		BASE64Decoder decoder = new BASE64Decoder();
		return decoder.decodeBuffer(data);
	}

	public static String byteToBase64(byte[] data){
		BASE64Encoder endecoder = new BASE64Encoder();
		return endecoder.encode(data);
	}
	
	public String getSalt() {
		return salt;
	}

	public String getDigest() {
		return digest;
	}
	
}
