package httpserver;

import java.sql.SQLException;
import java.util.Map;

import client.YoutubeVideo;

import logic.JsonResponse;
import logic.Logic;
import logic.SearchVideo;
import logic.ResponseData.VideoResponseData;

import java.util.List;

import database.DBConnection;
import database.Query.AddVideoQuery;
import database.Query.VideoExistsQuery;

public class AjaxSearchVideo extends AjaxParser {
	
	private String searchTerm;
	private int maxResults;
	private List<YoutubeVideo> videos = null;
	private String response;
	private DBConnection db;
	private SearchVideo searchVideo;
	
	public AjaxSearchVideo(Server server, Logic logic, Map<String, String> map) {
		super(server, logic, map);
		this.searchTerm = map.get("st");
		this.maxResults = 6;
		this.db = logic.getDb();
		this.searchVideo = new SearchVideo(searchTerm, maxResults);
		this.videos = searchVideo.getVideos();
		
		try {
			for (YoutubeVideo ytv : videos) {
				if (!videoExists(ytv.getVideoId())) {
					addVideoToDB(db, ytv);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		VideoResponseData responseData = new VideoResponseData(videos);
		JsonResponse resp = new JsonResponse(getCallback(), responseData);
		this.response = resp.getResponse();

	}
	
	public String getSearchTerm() {
		return searchTerm;
	}
	
	public String getResponse() {
		return response;
	}
	
	public List<YoutubeVideo> getVideos() {
		return videos;
	}
	
	public boolean videoExists(String vid) {
		try {
			VideoExistsQuery query = new VideoExistsQuery(db.getDbConnection(), vid);
			query.execute();
			query.getResults().next();
			boolean b = query.getResults().getBoolean(1);
			query.close();
			return b;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private void addVideoToDB(DBConnection db, YoutubeVideo ytv) {
		AddVideoQuery avq = new AddVideoQuery(db.getDbConnection(), ytv.getThumbnails().get(1), ytv.getEmbeddedWebPlayerUrl(), ytv.getDescription(), ytv.getTitle(), ytv.getDuration(), getSearchTerm(), ytv.getVideoId());
		avq.execute();
		avq.close();
	}

}
