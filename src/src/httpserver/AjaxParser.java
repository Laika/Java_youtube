package httpserver;

import java.util.Map;
import logic.*;

public class AjaxParser {
	
	private String callback;
	private String type;
	
	private Logic logic;
	private Server server;
	
	public AjaxParser(Server server, Logic logic, Map<String, String> map) {
			this.logic = logic;
			this.server = server;
			this.callback = map.get("callback");
			this.type = map.get("t");
	}
	
	public String getCallback() {
		return callback;
	}
	
	public String getType() {
		return type;
	}
	
}

