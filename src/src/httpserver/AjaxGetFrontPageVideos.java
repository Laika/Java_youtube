package httpserver;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import database.DBConnection;
import database.Query.GetFrontPageVideosQuery;
import database.Query.GetNumberOfVideosQuery;

import logic.JsonResponse;
import logic.Logic;
import logic.SearchVideo;
import logic.VideoData;
import logic.ResponseData.FrontPageVideosResponseData;

public class AjaxGetFrontPageVideos extends AjaxParser {
	
	private String response;
	private DBConnection db;
	private int videoCount;

	public AjaxGetFrontPageVideos(Server server, Logic logic,
			Map<String, String> map) {
		super(server, logic, map);
		
		this.db = logic.getDb();
		this.videoCount = numberOfVideos();

		GetFrontPageVideosQuery q = new GetFrontPageVideosQuery(db.getDbConnection());
		
		if (videoCount >= 6) {
			try {
				q.execute();
				
				ArrayList<VideoData> videos = new ArrayList<VideoData>();
				
				// if we fail to get 6 videos we should raise NotEnoughVideos
				final ResultSet results = q.getResults();
				
				while (q.getResults().next()) {
					final String thumbnailUrl = results.getString(2);
					final String videoUrl = results.getString(3);
					final String description = results.getString(4);
					final String title = results.getString(5);
					final String duration = results.getString(6);
					final String searchTerm = results.getString(7);
					final String videoId = results.getString(8);
					
					videos.add(new VideoData(thumbnailUrl, videoUrl, description, title, duration, searchTerm, videoId));
				}
				
				FrontPageVideosResponseData responseData = 
						new FrontPageVideosResponseData(videos.toArray(new VideoData[videos.size()]));
				JsonResponse resp = new JsonResponse(getCallback(), responseData);
				
				this.response = resp.getResponse();
			}  catch (SQLException e) {
				e.printStackTrace();
			} finally {
				q.close();
			}
		} else {
			SearchVideo s = new SearchVideo("Kittens");
			ArrayList<VideoData> videos = s.getVideoData();
			FrontPageVideosResponseData responseData = 
					new FrontPageVideosResponseData(videos.toArray(new VideoData[videos.size()]));
			JsonResponse resp = new JsonResponse(getCallback(), responseData);
			this.response = resp.getResponse();
		}
	}

	public String getResponse() {
		return this.response;
	}
	
	private int numberOfVideos() {
		int n = 0;
		GetNumberOfVideosQuery q = new GetNumberOfVideosQuery(db.getDbConnection());
		
		try {
			q.execute();
			final ResultSet r = q.getResults();
			r.next();
			n = r.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			q.close();
		}
		
		return n;
	}

}
