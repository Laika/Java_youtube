package httpserver;

import com.sun.net.httpserver.*;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import logic.Logic;

public class Server {
		
	public HttpServer server;
	
	public void startServer(Logic logic) throws Exception {
		server = HttpServer.create(new InetSocketAddress(8000), 0);
		server.createContext("/", new Handler(this, logic));
		server.setExecutor(null);
		server.start();
		System.out.println("Server is running.");
	}
	
	public void stopServer() {
		server.stop(0);
	}
	
	public static void writeResponse(HttpExchange t, String response) throws IOException {
		LinkedList<String> ctype = new LinkedList<String> ();
		ctype.add("text/javascript; charset=utf8");
		
		Map<String, List<String>> headers = t.getResponseHeaders();
		headers.put("Content-Type", ctype);
		
		t.sendResponseHeaders(200, response.length());
		
		OutputStream os = t.getResponseBody();
		os.write(response.getBytes());
		
		os.close();
		t.close();
	}
	
	public Map<String, String> queryToMap(String query) {
		Map<String, String> result = new HashMap<String, String>();
		
		for (String param : query.split("&")) {
			String pair[] = param.split("=");
			result.put(pair[0], pair[1]);
			
			if (pair.length > 1) {
				result.put(pair[0], pair[1]);
			} else {
				result.put(pair[0], "");
			}
		}
		return result;
	}
}
