package httpserver;

import java.util.Map;
import logic.*;
import logic.Exceptions.UserExistsException;

public class AjaxRegisterUser extends AjaxParser {
	private String user;
	private String salt;
	private String password;
	private String response;

	public AjaxRegisterUser(Server server, Logic logic, Map<String, String> map) throws UserExistsException {
		super(server, logic, map);
		this.user = map.get("u");
		this.salt = map.get("s");
		this.password = map.get("p");
		
		UserManager manager = logic.getUserManager();
		
		manager.addUser(this.user, this.password, this.salt);
	}
	
	public String getUser() {
		return this.user;
	}
	
	public String getSalt() {
		return this.salt;
	}
	
	public String getPassword() {
		return this.password;
	}

	public String getResponse() {
		return this.response;
	}
}
