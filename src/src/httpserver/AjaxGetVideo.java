package httpserver;

import java.sql.SQLException;
import java.util.Map;

import database.DBConnection;
import database.Query.GetClientSaltQuery;
import database.Query.GetVideoQuery;

import logic.JsonResponse;
import logic.Logic;
import logic.UserManager;
import logic.Exceptions.NoSuchUserException;
import logic.ResponseData.GetClientSaltResponseData;
import logic.ResponseData.VideoResponseData;

public class AjaxGetVideo extends AjaxParser {
	
	private String response;
	private int id;
	
	public AjaxGetVideo(Server server, Logic logic, Map<String, String> map) {
		super(server, logic, map);
		this.id = Integer.parseInt(map.get("i"));
		DBConnection db = logic.getDb();
		GetVideoQuery videoQuery = new GetVideoQuery(db.getDbConnection(), id);

		try {
			videoQuery.execute();
			videoQuery.getResults().next();
			String thumbnailUrl = videoQuery.getResults().getString(2);
			String videoUrl = videoQuery.getResults().getString(1);
			VideoResponseData responseData = new VideoResponseData(thumbnailUrl, videoUrl);
			JsonResponse resp = new JsonResponse(getCallback(), responseData);
			this.response = resp.getResponse();		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			videoQuery.close();
		}
	}
	
	public String getResponse() {
		return response;
	}
}
